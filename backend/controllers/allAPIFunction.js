import path from "path";
import { readFileSync, writeFileSync } from "fs";
const __dirname = path.resolve();

const fileReader = (fileName) => {
    let data = readFileSync(path.join(__dirname, `./Data/${fileName}`));
    data = data.toString();
    data = JSON.parse(data);
    return data;
};

const fileWriter = (fileName, dataToWrite) => {
    writeFileSync(path.join(__dirname, `./Data/${fileName}`), JSON.stringify(dataToWrite, null, "\t"));
};

const makeid = () => {
    var text = "";
    var possible = "0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

const allItems = () => {
    const data = fileReader("products2.json");
    return data;
};

export const getAllProducts = (req, res) => {
    let data = allItems();
    let dataToSend = data;
    // let dataToSend = data.response.data;
    res.send(dataToSend);
};

export const createProduct = (req, res) => {
    const productData = req.body;
    const data = allItems();
    const dataToAppend = { ...productData, id: makeid() };
    data.push(dataToAppend);
    fileWriter("products2.json", data);
    res.send("Product Added");
}

export const getProduct = (req, res) => {
    const allProducts = allItems();
    const singleProduct = allProducts.find((product) => product.id === req.params.id);

    res.send(singleProduct);
}

export const deleteProduct = (req, res) => {
    const allProducts = allItems();
    const dataToSend = allProducts.filter((product) => product.id !== req.params.id);
    fileWriter("products2.json", dataToSend);
    res.send("Product Deleted");
}

export const updateProduct = (req, res) => {
    const allProducts = allItems();

    const singleProduct = allProducts.find((product) => product.id === req.params.id);

    singleProduct.name = req.body.name;
    singleProduct.quantity = req.body.quantity;
    singleProduct.url = req.body.url;
    singleProduct.price = req.body.price;
    singleProduct.description = req.body.description;

    fileWriter("products2.json", allProducts);
    res.send("Updation Successfull");
}

export const getForMenuAll = (req, res) => {
    const allProducts = allItems();
    let dataToSend = allProducts.map((product) => {
        let temp = {};
        temp.name = product.name;
        temp.price = product.price;
        temp.url = product.url;
        temp.id = product.id;

        return temp;
    })
    res.send(dataToSend);
}

export const sendDataToCart = (req, res) => {
    const cartData = req.body;
    fileWriter("cart.json", cartData);
    res.send("Cart Updated");
}

export const addOrder = (req, res) => {
    const orderData = req.body;
    const data = fileReader("orders.json");
    const dataToAppend = {}
    dataToAppend[makeid()] = { ...orderData };
    let dataToWrite = Object.assign(data, dataToAppend);
    fileWriter("orders.json", dataToWrite);
    res.send("Order Added");
}

export const getAllOrder = (req, res) => {
    const allOrders = fileReader("orders.json");
    let dataToSend = Object.keys(allOrders).map((order) => {
        let temp = {};
        temp.totalPrice = allOrders[order]['all-total'];
        temp.id = order;
        temp.date = allOrders[order]['date']
        return temp;
    })
    res.send(dataToSend);
}

export const getOrderById = (req, res) => {
    const allOrders = fileReader("orders.json");
    const singleOrder = allOrders[req.params.id];

    res.send(singleOrder);
}

export const getCart = (req, res) => {
    const allCart = fileReader("cart.json");

    res.send(allCart);
}

export const getProductByIdMini = (req, res) => {
    const allProducts = allItems();
    const singleProduct = allProducts.find((product) => product.id === req.params.id)

    let temp = {};
    temp.name = singleProduct.name;
    temp.price = singleProduct.price;
    temp.id = singleProduct.id;

    res.send(temp);
}