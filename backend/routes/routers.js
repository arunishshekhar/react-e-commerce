import express from "express";
import { createProduct, getAllProducts, getProduct, deleteProduct, 
    updateProduct, getForMenuAll, sendDataToCart, addOrder, getAllOrder,
    getOrderById, getCart, getProductByIdMini } from "../controllers/allAPIFunction.js";


const router = express.Router();

router.get("/products",getAllProducts);
router.post("/add-product",createProduct);
router.get("/product/:id",getProduct);
router.delete("/product/:id",deleteProduct);
router.put("/product/:id",updateProduct);
router.get("/menu-all", getForMenuAll);
router.post("/cart-work",sendDataToCart);
router.post("/add-order",addOrder);
router.get("/all-order",getAllOrder);
router.get("/order/:id",getOrderById);
router.get("/get-cart",getCart);
router.get("/product-mini/:id",getProductByIdMini);

export default router;