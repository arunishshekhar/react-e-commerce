# How to Install
## This Project contain 2 npm instances one in Folder Frontend other in Backend.
- Install both with `npm` command

# How to run
- It requires 2 terminal opened one in Frontend other in Backend folder.
- First Start Backend with `npm run server` it runs on port 5000 make it free if it is occupied.

- In Frontend folder run `npm start`

# Thank You
