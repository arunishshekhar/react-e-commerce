import axios from "axios";
import React from "react";


const AppContext = React.createContext({
    cartList: {},
    increment: () => { },
    decrement: () => { },
    addToCartHandler: () => { },
    storeToCart: () => { },
    total: 0,
    placeOrderHandler: () => { },
    deleteFromCart: () => { }
});

class AppContextClass extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            total: 0,
            cart: {}
        };
        axios.get("http://localhost:5000/get-cart")
            .then((data) => {
                this.setState({
                    cart: (data.data.cart) ? data.data.cart : {},
                    total: (data.data.total) ? data.data.total : 0
                })
            })

            this.deleteFromCart = this.deleteFromCart.bind(this);
    }

    increment = (productID, price) => {

        this.setState(prevState => ({
            cart: {
                ...prevState.cart,
                [productID]: prevState.cart[productID] + 1
            },
            total: prevState.total + price
        }))
        this.storeToCart();
    }

    decrement = (productID, price) => {

        this.setState(prevState => ({
            cart: {
                ...prevState.cart,
                [productID]: prevState.cart[productID] - 1
            },
            total: prevState.total - price
        }))
        this.storeToCart();
    };

    addToCartHandler = (product) => {
        let tempVal = {};
        tempVal[product.id] = 1;
        (this.state.total) ? this.state.total += product.price : this.state.total = product.price;
        this.setState(Object.assign(this.state.cart, tempVal));
        this.storeToCart();
    }

    storeToCart = () => {
        axios.post("http://localhost:5000/cart-work", this.state);
    }

    placeOrderHandler = () => {
        if (this.state.total <= 0 || !this.state.total) {
            return
        }
        else {
            axios.post("http://localhost:5000/cart-work", {});
            let dataToSend = {};
            dataToSend["all-total"] = this.state.total;
            let now = new Date();
            dataToSend["date"] = now;
            dataToSend["cart"] = this.state.cart;
            this.setState({
                cart: {},
                total: 0
            })
            axios.post("http://localhost:5000/add-order", dataToSend);
        }
    }

    deleteFromCart = (productID) => {
        let prevCart = this.state.cart;
        delete prevCart[productID]
        this.setState({
            cart: prevCart
        })
        this.storeToCart();

    }


    render() {
        return (
            <AppContext.Provider value={{
                cartList: this.state.cart,
                increment: this.increment,
                decrement: this.decrement,
                addToCartHandler: this.addToCartHandler,
                storeToCart: this.storeToCart,
                total: this.state.total,
                placeOrderHandler: this.placeOrderHandler,
                deleteFromCart: this.deleteFromCart
            }}>
                {this.props.children}
            </AppContext.Provider>
        )
    }
}

export { AppContext };
export default AppContextClass;