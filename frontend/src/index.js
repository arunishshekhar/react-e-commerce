import React from 'react';
import ReactDOM from 'react-dom/client';
import AppContextClass, { AppContext } from './context';
import { Routes, Route, Link, BrowserRouter } from 'react-router-dom';
import "./index.css"


import MenuClass from './components/menu/menu';
import Cart from "./components/carts/cart";
import Orders from "./components/Orders/orders"
import SingleProduct from './components/detail/product';
import SingleOrder from './components/detail/order'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <AppContextClass>
        <BrowserRouter>
            <AppContext.Consumer>
                {
                    (contextProps) => {
                        return <div className='nav-bar'>
                            <Link to="/" onClick={() => contextProps.storeToCart()}>Home</Link>
                            <Link to="/cart" onClick={() => contextProps.storeToCart()}>Carts</Link>
                            <Link to="/orders">Orders</Link>
                        </div>
                    }
                }
            </AppContext.Consumer>
            <Routes>
                <Route path="/" element={<MenuClass />} />
                <Route path="/cart" element={<Cart />} />
                <Route path="/orders" element={<Orders />} />
                <Route path="/product/:id" element={<SingleProduct/>} />
                <Route path="/order/:id" element={<SingleOrder/>} />
            </Routes>
        </BrowserRouter>
    </AppContextClass>
);