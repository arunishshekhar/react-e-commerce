import axios from "axios";
import React from "react";
import "./single-product.css"

class SingleProductDisplay extends React.Component {
    constructor(props) {
        super(props)

        this.state={}

        axios.get(`http://localhost:5000/product-mini/${this.props.id}`)
        .then((data) => this.setState(data.data))
    }

    render() {
        return (
            <section className="indi-section">
                <h1>Name: {this.state.name}</h1>
                <p>Price: {this.state.price}</p>
                <p>Quantity: {this.props.quant}</p>
                <p>Total: {this.state.price * this.props.quant}</p>
                
            </section>
        )
    }
}

export default SingleProductDisplay