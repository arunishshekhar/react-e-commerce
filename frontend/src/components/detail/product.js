import axios from "axios";
import React from "react";
import { useParams } from 'react-router-dom'
import "./product.css"

class SingleProduct extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }

        axios.get(`http://localhost:5000/product/${this.props["para"]}`)
            .then((data) => this.setState(data.data))
    }

    render() {
        return (
            <div className="main-div">
                <div className="image-div">
                    <img src={this.state.url} />
                </div>
                <div className="product-data">
                    <h1>Name: {this.state.name}</h1>
                    <p>Description: {this.state.description}</p>
                    <p>Price: {this.state.price}</p>
                </div>
            </div>
        )
    }
}

const GetID = () => {
    const { id } = useParams();
    return <SingleProduct para={id} />

}
export default GetID;