import axios from "axios";
import React from "react";
import { useHistory, useParams } from 'react-router-dom'
import SingleProductDisplay from "./single-product";
import "./orders.css"

class SingleOrder extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }

        axios.get(`http://localhost:5000/order/${this.props["para"]}`)
            .then((data) => this.setState(data.data))
    }


    render() {
        return (
            <div className="main-div">
                <div>
                    <h2>Order Id: {this.props["para"]}</h2>
                    <h2>Net total: {this.state["all-total"]}</h2>
                    {/* <h1>Date: {this.state["date"]}</h1> */}
                </div>
                <div>
                    {Object.keys(this.state["cart"] || {}).map((id) => {
                        return <SingleProductDisplay key={id} id={id} quant={this.state["cart"][id]} />
                    })}
                </div>
            </div>
        )
    }
}

const GetID = () => {
    const { id } = useParams();
    return <SingleOrder para={id} />

}
export default GetID;