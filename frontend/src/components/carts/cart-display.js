import axios from "axios";
import React from "react";
import { AppContext } from '../../context';
import "./cart-display.css"


class CartDisplay extends React.Component {
    constructor(props) {
        super(props)


        this.state = {
        }

        axios.get(`http://localhost:5000/product-mini/${this.props.data[0]}`)
            .then((data) => this.setState(data.data))

    }

    render() {
        return (
            <section className="card-indi-order">
                <div>
                    <p>{this.state.name}</p>
                    <p>Price: {this.state.price}</p>
                </div>
                <div>
                    <p>Total: {this.state.price * this.props.data[1]}</p>
                    <AppContext.Consumer>
                        {
                            (contextProps) => {
                                if (!contextProps.cartList[this.props.data[0]] || contextProps.cartList[this.props.data[0]] === 0) {
                                    {contextProps.deleteFromCart(this.props.data[0])}
                                    return 
                                }
                                else {
                                    return <div id="inc-dec-div">
                                        <div className="op-inc-dec" onClick={() => contextProps.decrement(this.props.data[0],this.state.price)}>-</div>
                                        <div>{contextProps.cartList[this.props.data[0]]}</div>
                                        <div className="op-inc-dec" onClick={() => contextProps.increment(this.props.data[0],this.state.price)}>+</div>
                                    </div>
                                }
                            }
                        }
                    </AppContext.Consumer>
                </div>
            </section>
        )
    }
}

export default CartDisplay;