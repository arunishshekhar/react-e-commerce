import React from "react";
import { AppContext } from '../../context';
import CartDisplay from "./cart-display";
import "./cart.css"

class Cart extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
        }
    }

    render() {
        return (
            <div>
                <AppContext.Consumer>
                    {
                        (contextProps) => {
                            return (
                                <div className="main-div">
                                    <div className="allCartItems">
                                        {Object.entries(contextProps.cartList).map((orders) => <CartDisplay key={orders[0]} data={orders} />)}
                                    </div>
                                    <div>
                                        {
                                            Object.keys(contextProps.cartList).length > 0 ? <button className="place-order" onClick={() => {
                                                contextProps.placeOrderHandler();
                                            }} >Place Order  {contextProps.total}</button> : <h1>Cart is empty</h1>
                                        }
                                        
                                    </div>
                                </div>
                            )
                        }
                    }
                </AppContext.Consumer>
            </div>
        )
    }
}

export default Cart;