import axios from "axios";
import React from "react";
import MenuDisplay from "./menu-display"
import "./menu.css"


class MenuClass extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allProduct: []
        }
        axios.get("http://localhost:5000/menu-all").then((valueReturned) => {
            this.setState({
                allProduct: valueReturned.data
            });
        });
    }

    render() {
        return (
            <div className="allProduct">
                {this.state.allProduct.map((indivisualProduct) =>
                    <MenuDisplay key={indivisualProduct.id} productData={indivisualProduct} />)}
            </div>

        )
    }
}

export default MenuClass;