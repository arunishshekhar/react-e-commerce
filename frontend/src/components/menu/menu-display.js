import React from "react";
import "./display-items.css";
import { AppContext } from '../../context';


class MenuDisplay extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            cart: {}
        }

    }



    render() {
        return (
            <section className="indivisualProductSection" key={this.props.productData.id}>
                <img src={this.props.productData.url} />
                <a href={`http://localhost:3000/product/${this.props.productData.id}`}><div className="data-div">
                    <h3>{this.props.productData.name}</h3>
                    <span>Price: {this.props.productData.price}</span>
                </div></a>
                <div className="cart-op-div">
                    <AppContext.Consumer>
                        {
                            (contextProps) => {
                                if (!contextProps.cartList[this.props.productData.id] || contextProps.cartList[this.props.productData.id] === 0) {
                                    delete contextProps.cartList[this.props.productData.id];
                                    return <button id="add-to-cart" onClick={() => contextProps.addToCartHandler(this.props.productData)}>Add To Cart</button>
                                }
                                else {
                                    return <div id="inc-dec-div">
                                        <div className="op-inc-dec" onClick={() => contextProps.decrement(this.props.productData.id, this.props.productData.price)}>-</div>
                                        <div>{contextProps.cartList[this.props.productData.id]}</div>
                                        <div className="op-inc-dec" onClick={() => contextProps.increment(this.props.productData.id, this.props.productData.price)}>+</div>
                                    </div>
                                }
                            }
                        }
                    </AppContext.Consumer>
                </div>
            </section>
        )
    }
}

export default MenuDisplay;