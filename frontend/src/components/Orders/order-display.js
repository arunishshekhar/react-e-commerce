import React from "react";
import "./order-display.css";

class OrderDisplay extends React.Component {
    constructor(props){
        super(props)

        this.state = {}
    }

    render () {
        return (
            <a href={`http://localhost:3000/order/${this.props.para[1].id}`}><section className="indi-order">
                <p>OrderID : {this.props.para[1].id}</p>
                {/* <p>Date : {this.props.para[1].date}</p> */}
                <p>Total : {this.props.para[1].totalPrice}</p>
            </section></a>
        )
    }
}

export default OrderDisplay;