import axios from "axios";
import React from "react";
import OrderDisplay from "./order-display";
import "./orders.css"

class Orders extends React.Component {

    constructor(props) {
        super(props)

        this.state = {}

        axios.get("http://localhost:5000/all-order")
        .then((data) => {this.setState(data.data)})
    }
    render() {
        return (
            <div className="all-orders">
                {Object.entries(this.state).map((order) => <OrderDisplay key= {order[1].id}para = {order}/>)}
            </div>
            
        )
    }
}

export default Orders;